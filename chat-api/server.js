const express = require('express');
const cors = require('cors');
const db = require('./db/fileDb');
const chat = require('./app/chat');

db.init();

const app = express();
app.use(cors());
app.use(express.json());

const port = 8000;

app.use('/chat', chat);

app.listen(port, () => {
    console.log('Our port ' + port)
});