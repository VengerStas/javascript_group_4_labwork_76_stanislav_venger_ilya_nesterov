const express = require('express');
const nanoid = require('nanoid');

const db = require('../db/fileDb');

const router = express.Router();

router.get('/', (req, res) => {
    res.send(db.getMessages(req.query.datetime));
});

router.post('/', (req, res) => {
    const datetime = new Date().toISOString();
    const messages = {
        author: req.body.author,
        message: req.body.message,
        datetime: datetime
    };
    messages.id = nanoid();
    if (messages.author === '' || messages.message === '') {
        res.status(400).send(JSON.stringify({"error": "Author or message must be present in the request"}, null, 2))
    } else {
        res.send("OK");
        db.addMessage(messages);
    }
    const dateNew = new Date(datetime);
    if (isNaN(dateNew.getDate())) {
        res.status(400).send(JSON.stringify({"error": "incorrect Date"}, null, 2))
    }
});

module.exports = router;
