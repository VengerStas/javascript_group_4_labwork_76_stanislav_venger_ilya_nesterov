const fs = require('fs');

const filename = './db.json';

let chat = [];

module.exports = {
    init () {
        try {
            const fileContents = fs.readFileSync(filename);
            chat = JSON.parse(fileContents);
        } catch (e) {
            chat = [];
        }
    },
    getMessages(date) {
        if(date) {
            return chat.filter(message => message.datetime > date)
        } else {
            return chat.slice(-30);
        }
    },
    addMessage(messages) {
        chat.push(messages);
        this.save();
    },
    save () {
        fs.writeFileSync(filename, JSON.stringify(chat, null, 2))
    }
};

