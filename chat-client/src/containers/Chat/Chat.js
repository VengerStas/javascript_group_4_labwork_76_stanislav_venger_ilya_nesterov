import React, {Component} from 'react';
import AddMessage from "../../components/AddMessage/AddMessage";
import {Card, CardBody, CardText, CardTitle, Container} from "reactstrap";
import {connect} from "react-redux";
import {fetchGetMessages} from "../../store/actions";

import './Chat.css';

let interval;

class Chat extends Component {
    componentDidMount() {
        this.props.getMessages();
    }

    componentDidUpdate() {
        clearInterval(interval);
        interval = setInterval(() => {
            const lastData = this.props.messages[this.props.messages - 1];
            this.props.getMessages(lastData);
        }, 2000)
    }

    render() {
        let chatMessage = this.props.messages.map(message => (
            <Card key={message.id}>
                <CardBody>
                    <CardTitle><strong>Name: </strong>{message.author}</CardTitle>
                    <CardText><strong>Message: </strong>{message.message}</CardText>
                </CardBody>
            </Card>
        ));
        return (
            <div className="chat">
                <Container>
                    <AddMessage/>
                    <div className="chat-block">
                        {chatMessage}
                    </div>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    messages: state.messages
});

const mapDispatchToProps = dispatch => ({
    getMessages: () => dispatch(fetchGetMessages())
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
