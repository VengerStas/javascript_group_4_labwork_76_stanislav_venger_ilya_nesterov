import React, {Component} from 'react';
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {changeValue, fetchPostMessage} from "../../store/actions";

import './AddMessage.css';

class AddMessage extends Component {
    submitFormHandler = event => {
        event.preventDefault();
        const data = {
            author: this.props.state.author,
            message: this.props.state.message,
        };
        this.props.sendMessage(data);
    };

    render() {
        return (
            <div className="send-form">
                <Form className="form">
                    <FormGroup>
                        <Label>Name:</Label>
                        <Input type="text"  name="author" value={this.props.state.author} onChange={this.props.changeValue} placeholder="Enter your name"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Message:</Label>
                        <Input type="text"  name="message" value={this.props.state.message} onChange={this.props.changeValue} placeholder="Enter your message"/>
                    </FormGroup>
                    <Button type="submit" onClick={(event) => this.submitFormHandler(event)}>Send</Button>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    state: state
});

const mapDispatchToProps = dispatch => ({
    changeValue: (event) => dispatch(changeValue(event)),
    sendMessage: (author, message) => dispatch(fetchPostMessage(author, message))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddMessage);
