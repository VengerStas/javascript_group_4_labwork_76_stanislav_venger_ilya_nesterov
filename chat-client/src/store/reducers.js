import {CHANGE_VALUE, MESSAGE_REQUEST_SUCCESS} from "./actions";

const initialState = {
    messages: [],
    author: '',
    message: '',
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_VALUE:
            return {
                ...state,
                [action.event.target.name]: action.event.target.value
            };
        case MESSAGE_REQUEST_SUCCESS:
            return {
                ...state,
                messages: action.messages
            };
        default:
            return state;
    }
};

export default reducer;
