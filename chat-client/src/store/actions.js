import axios  from '../axios-chat';

export const MESSAGE_REQUEST = "MESSAGE_REQUEST";
export const MESSAGE_REQUEST_SUCCESS = "MESSAGE_REQUEST_SUCCESS";
export const MESSAGE_FAILURE = "MESSAGE_FAILURE";
export const CHANGE_VALUE = "CHANGE_VALUE";


export const changeValue = event => ({type: CHANGE_VALUE, event});

export const messageRequest = () => {
    return {type: MESSAGE_REQUEST};
};

export const messageSuccess = messages => {
    return {type: MESSAGE_REQUEST_SUCCESS, messages};
};

export const messageFailure = error => {
    return {type: MESSAGE_FAILURE, error};
};

export const fetchGetMessages = (date) => {
    return (dispatch) => {
        dispatch(messageRequest());
        const url = '/chat';
        const dateUrl = `/chat?datetime=${date}`;
        return axios.get(date ? dateUrl : url).then(response => {
            dispatch(messageSuccess(response.data));
        }, error => {
            dispatch(messageFailure(error));
        })
    }
};

export const fetchPostMessage = (data) => {
    return (dispatch) => {
        dispatch(messageRequest());
        axios.post('/chat', data).then (() => {
            dispatch(messageRequest());
        }, error => {
            dispatch(messageFailure(error));
        })
    }
};
